import EditTeamTopbar from '../editTeamPage/EditTeamTopbar'
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import '../../styles/componentStyles/EditTeamPage/EditTeamPageStyles.css'
import { useState } from 'react';
import useFetch from '../../hooks/useFetch';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';

import FormationSubPage from '../editTeamPage/subPages/formation/FormationSubPage';
import PlayersSubPage from '../editTeamPage/subPages/players/PlayersSubPage';
import MoreSubPage from '../editTeamPage/subPages/more/MoreSubPage';


const EditTeamPage = () => {

    const { teamId } = useParams()

    const UpdateTeamData = (updateInfo) => {
        // gets updateInfo in this format: [{'field': *name of field*, 'updatedValue': *the new value*}, ...]
        // set the state teamData with the new value

        updateInfo.map((fieldObject) => {
            let field = fieldObject.field // field to edit
            let updatedValue = fieldObject.updatedValue // new value of the field

            let teamDataCopy = {...teamData}
            teamDataCopy.team_data[field] = updatedValue

            setData(teamDataCopy) // updating teamData
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setRequestPostData({'team_id': teamId, 'updated_data': teamData})
        setAfterFunctions([{'function': RedirectToTeams, 'params': [], 'fetchParams': []}])
        setRequestUrl('/set_team_data')        
    }

    const RedirectToTeams = () => {
        history.push('/teams')
    }

    const GetSubPageComponent = () => {
        if (currentSubPage === 'formation') {
            return <FormationSubPage data={{'teamData': teamData, 'UpdateTeamData': UpdateTeamData}} /> // passing the teamData to formationSubPage
        }
        if (currentSubPage === 'players') {
            return <PlayersSubPage data={{'teamData': teamData, 'UpdateTeamData': UpdateTeamData}} />
        }
        if (currentSubPage === 'more') {
            return <MoreSubPage data={{'teamData': teamData, 'UpdateTeamData': UpdateTeamData}} />
        }
    }

    const history = useHistory()

    // fetching team data
    const [requestUrl, setRequestUrl] = useState('/get_team_data')
    const [requestPostData, setRequestPostData] = useState({'team_id': teamId, 'fields': ['team_id', 'name', 'formation', 'players', 'roles', 'positions']}) 
    const [afterFunctions, setAfterFunctions] = useState([]) 
    const {data: teamData, setData, isPending} = useFetch(requestUrl, requestPostData, afterFunctions)
    
    const [currentSubPage, setCurrentSubPage] = useState('formation') // contain current subPage, possible values are 'formation' or 'players' or 'more'
    const editTeamTopbarProps = {'currentSubPage': currentSubPage, 'setCurrentSubPage': setCurrentSubPage}


    return (
        <div className="edit-team-page-component simple-page-component">
            
            <EditTeamTopbar data={editTeamTopbarProps} />

            {!isPending && 
                <form className='edit-team-form' onSubmit={(e) => handleSubmit(e)}>
                    <input
                        className="simple-text-input team-name-text-input"
                        type="text"
                        required
                        value={teamData.team_data.name} 
                        onChange={(e) => UpdateTeamData([{'field': 'name', 'updatedValue': e.target.value}])} />

                    {GetSubPageComponent(currentSubPage)}

                    <button>submit changes</button>

                </form>
            }

        </div>
    );
}
 
export default EditTeamPage;