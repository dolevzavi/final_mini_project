import '../../styles/componentStyles/TeamsPage/TeamsPageStyles.css';
import TeamsTable from '../TeamsPage/TeamsTable';
import useFetch from '../../hooks/useFetch'
import { useState } from 'react';
import CreateTeamModal from '../modals/CreateTeamModal';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';


const TeamsPage = () => {

    const history = useHistory()

    // runs when TeamsTable is rendered (after the fetch)
    const GetTeamsTableProps = (data) => {
        return {'teamsList': data.teams_list, 'setTeamsList': updateData} // props for TeamsTable
    }

    const updateData = (newData) => {
        setData({'teams_list': newData})
    }

    const OpenCreateTeamModal = (e) => {
        e.preventDefault()
        SetShowCreateTeamModal(true)
    }

    const CloseCreateTeamModal = (e) => {
        e.preventDefault()
        SetShowCreateTeamModal(false)
    }

    const CreateTeam = (e, teamName) => {
        e.preventDefault()

        // sending create_new_team request to the server
        SetCreateTeamPostData({'name': teamName})
        SetCreateTeamUrl('/')
        setTimeout(() => {
            SetCreateTeamUrl('/create_new_team')
        }, 250)
        
        CloseCreateTeamModal(e)

        // sending the server new '/get_teams' request (timeout for the requestUrl to refresh)
        setRequestUrl('/')
        setTimeout(() => {
            setRequestUrl('/get_teams')
        }, 500)
    }

    // fetching - create new team
    const [createTeamUrl, SetCreateTeamUrl] = useState('/')
    const [createTeamPostData, SetCreateTeamPostData] = useState()
    const {data: createTeamData, setData: createTeamSetData, isPending: createTeamIsPending} = useFetch(createTeamUrl, createTeamPostData, [])

    // fetching - teams data
    const [requestUrl, setRequestUrl] = useState('/get_teams') // the url sent to server, (each time it will be set to a new value a request will occur)
    const {data, setData, isPending } = useFetch(requestUrl, {}, []) // teams data fetched from server, should look like this {'teams_list': [{'name': 'team's_name', 'rating': 'team's_rating', 'team_id': 'team's_id'}...]}

    const [showCreateTeamModal, SetShowCreateTeamModal] = useState(false)
    const createTeamModalProps = {'CloseCreateTeamModal': CloseCreateTeamModal, 'CreateTeam': CreateTeam}


    return (
        <div className="teams-page-component simple-page-component">
            
            <p className='teams-header center-container'>Teams</p>
            
            <div className='table-space'>
                <div className='create-team-button-container center-container'>
                    <button className='simple-button' onClick={(e) => OpenCreateTeamModal(e)}>create new team</button>
                </div>
                
                {!isPending && <TeamsTable data={GetTeamsTableProps(data)} />}
            </div>

            {showCreateTeamModal && <CreateTeamModal data={createTeamModalProps} />}

        </div>
    )
}

export default TeamsPage;