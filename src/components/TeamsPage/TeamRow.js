import '../../styles/componentStyles/TeamsPage/TeamRowStyles.css';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';

const TeamRow = (props) => {

    const { teamData, OpenRemoveTeamModal, teamRowColorIndex } = { ...props.data }
    console.log('this is teamRow: ', teamRowColorIndex)
    
    const GetRowClass = () => {
        if (teamRowColorIndex % 2 == 0) {
            return ' light-row-background '
        }
        else {
            return ' dark-row-background '
        }
    }

    return ( 
        <div className={"team-row-component" + GetRowClass() + 'center-container'}>

            {/* team name */}
            <div className='team-name-container center-container'>
                <p>{teamData.name}</p>
            </div>

            {/* team rating */}
            <div className='team-rating-container center-container'>
                <p>{teamData.rating}</p>
            </div>

            {/* edit button */}
            <div className='edit-button-container center-container'>
                <Link to={'/edit_team/' + teamData.team_id} >
                    <button className='simple-button'>edit</button>
                </Link>
            </div>

            {/* remove button */}
            <div className='remove-button-container center-container'>
                <button className='simple-button' onClick={() => OpenRemoveTeamModal(teamData.team_id)}>remove</button>
            </div>

        </div>
    );
}
 
export default TeamRow;