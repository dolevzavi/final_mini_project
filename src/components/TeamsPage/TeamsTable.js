import TeamRow from './TeamRow'
import RemoveTeamModal from '../modals/RemoveTeamModal'
import { useState } from 'react'
import useFetch from '../../hooks/useFetch'

import '../../styles/componentStyles/TeamsPage/TeamsTableStyles.css'



const TeamsTable = (props) => {

    const { teamsList, setTeamsList } = { ...props.data }

    // close RemoveTeamModal - run when clicking 'cancel' on the modal.
    const CloseRemoveTeamModal = () => {
        setShowRemoveTeamModal(false)
    }

    // opening the RemoveTeamModal - run when clicking 'edit' on a TeamRow which also passing the teamId
    const OpenRemoveTeamModal = (teamId) => {
        setRemoveTeamModalProps({'teamId': teamId, 'CloseRemoveTeamModal': CloseRemoveTeamModal, 'RemoveTeam': RemoveTeam})  // setting the props that the modal will get with the right ID
        setShowRemoveTeamModal(true)
    }

    // runs when you click on 'remove' in removeTeamModal, this function send the remove_team request to the server
    const RemoveTeam = (teamId) => {
        setRemoveTeamModalProps({'teamId': teamId, 'CloseRemoveTeamModal': CloseRemoveTeamModal, 'RemoveTeam': RemoveTeam, 'isPending': isPending}) // mainly because I want the modal to get isPending
        setRequestPostData({'teamId': teamId}) // team to remove
        setAfterFunctions([{'function': CloseRemoveTeamModal, 'params': [], 'fetchParams': []}, {'function': UpdateTeamsList, 'params': [teamId], 'fetchParams': []}]) // closing the modal after the fetch is ended
        
        // sending the remove request to the server (delay so requestUrl will refresh)
        setRequestUrl('/')
        setTimeout(() => {
            setRequestUrl('/remove_team') // sending the server the reomve_team request (by changing the url)
        }, 500)
    }

    const UpdateTeamsList = (deletedTeamId) => {
        // this function set teamsList without the team that was deleted
        let dataCopy = [ ...teamsList ]
        dataCopy = dataCopy.filter(item => item.team_id !== deletedTeamId)  
        setTeamsList(dataCopy)
    }

    // fetching
    const [requestUrl, setRequestUrl] = useState('/')
    const [requestPostData, setRequestPostData] = useState({})
    const [afterFunctions, setAfterFunctions] = useState([])
    const {data, setData, isPending} = useFetch(requestUrl, requestPostData, afterFunctions)

    const [showRemoveTeamModal, setShowRemoveTeamModal] = useState(false) // the state of the modal - shown/hidden
    const [removeTeamModalProps, setRemoveTeamModalProps] = useState({}); // props for RemoveTeamModal - setted inside OpenRemoveTeamModal()

    let teamRowColorIndex = 0;

    return (  
        <div className="teams-table-component">
            {teamsList && teamsList.map((teamData) => {
                teamRowColorIndex += 1;
                let teamRowProps = {'teamData': teamData, 'OpenRemoveTeamModal': OpenRemoveTeamModal, 'teamRowColorIndex': teamRowColorIndex} // the props for each TeamRow
                return (
                    <TeamRow data={teamRowProps} key={teamData.team_id} />
                )
            })}
            
            {showRemoveTeamModal &&
                <RemoveTeamModal data={removeTeamModalProps} />
            }
        </div>
    );
}
 
export default TeamsTable;