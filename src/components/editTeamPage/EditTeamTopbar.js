import '../../styles/componentStyles/EditTeamPage/EditTeamTopbarStyles.css'


const EditTeamTopbar = (props) => {

    const {currentSubPage, setCurrentSubPage} = {...props.data}

    return (
        <div className="edit-team-topbar-component">
            <div className="topbar-button-container center-container">
                <button onClick={() => setCurrentSubPage('formation')}>formation</button>
            </div>
            <div className="topbar-button-container center-container">
                <button onClick={() => setCurrentSubPage('players')}>players</button>
            </div>
            <div className="topbar-button-container center-container">
                <button onClick={() => setCurrentSubPage('more')}>more</button>
            </div>
        </div>
    );
}
 
export default EditTeamTopbar;