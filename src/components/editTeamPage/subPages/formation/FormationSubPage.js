import { cloneDeep } from 'lodash'
import { useState } from 'react'
import Formation from './Formation'
import ChangeFormationModal from '../../../modals/ChangeFormationModal'

const FormationSubPage = (props) => {

    const { teamData, UpdateTeamData } = {...props.data}

    const OpenChangeFormationModal = (e, formationId) => {
        e.preventDefault()
        SetChangeFormationModalProps({'CloseChangeFormationModal': CloseChangeFormationModal, 'ChangeFormation': ChangeFormation, 'formationId': formationId})
        SetShowChangeFormationModal(true)
    }

    const CloseChangeFormationModal = (e) => {
        e.preventDefault()
        SetShowChangeFormationModal(false)
    }

    const ChangeFormation = (e, formationId) => {
        e.preventDefault()
        
        // updating the 'formation' field of team
        let updatedFormation = possibleFormations.find(item => item.formation_id === formationId)

        // removing all players from team
        let emptyPositions = {'goalkeeper': [], 'defence': [], 'middlefield': [], 'attack': []}

        UpdateTeamData([
            {'field': 'formation', 'updatedValue': updatedFormation},
            {'field': 'players', 'updatedValue': []},
            {'field': 'positions', 'updatedValue': emptyPositions}
        ])

        CloseChangeFormationModal(e)
    }

    // similar to how formations looks like in DB (important)
    const possibleFormations = [
        {'formation_id': 0, 'name': '5-4-1', 'goalkeeper': 1, 'defence': 5, 'middlefield': 4, 'attack': 1}, 
        {'formation_id': 1, 'name': '5-3-2', 'goalkeeper': 1, 'defence': 5, 'middlefield': 3, 'attack': 2}, 
        {'formation_id': 2, 'name': '4-5-1', 'goalkeeper': 1, 'defence': 4, 'middlefield': 5, 'attack': 1}, 
        {'formation_id': 3, 'name': '4-4-2', 'goalkeeper': 1, 'defence': 4, 'middlefield': 4, 'attack': 2}, 
        {'formation_id': 4, 'name': '4-3-3', 'goalkeeper': 1, 'defence': 4, 'middlefield': 3, 'attack': 3}, 
        {'formation_id': 5, 'name': '4-2-4', 'goalkeeper': 1, 'defence': 4, 'middlefield': 2, 'attack': 4}, 
        {'formation_id': 6, 'name': '3-4-3', 'goalkeeper': 1, 'defence': 3, 'middlefield': 4, 'attack': 3}, 
        {'formation_id': 7, 'name': '3-5-2', 'goalkeeper': 1, 'defence': 3, 'middlefield': 5, 'attack': 2}, 
    ]

    const [showChangeFormationModal, SetShowChangeFormationModal] = useState(false)
    const [changeFormationModalProps, SetChangeFormationModalProps] = useState();
    

    return (
        <div className='formation-subpage-component'>
            {possibleFormations.map((formation) => {
                let formationProps = {'formationData': formation, 'currentFormationId': teamData.team_data.formation.formation_id, 'OpenChangeFormationModal': OpenChangeFormationModal}
                return <Formation data={formationProps} key={formation.formation_id} />
            })}

            {showChangeFormationModal && <ChangeFormationModal data={changeFormationModalProps} />}
        </div>
    );
}
 
export default FormationSubPage;