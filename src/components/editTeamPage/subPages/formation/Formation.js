import '../../../../styles/componentStyles/EditTeamPage/FormationStyles.css'


const Formation = (props) => {

    const {formationData, currentFormationId, OpenChangeFormationModal} = {...props.data}
    // console.log('this is formationData: ', formationData, 'currentFormationId', currentFormationId)

    const GetFormationClass = () => {
        if (formationData.formation_id === currentFormationId) {
            return 'formation-component selected-formation'
        }
        else {
            return 'formation-component'
        }
    }


    return ( 
        // <div className={GetFormationClass()} onClick={(e) => UpdateTeamData([{'field': 'formation', 'updatedValue': formationData}])}>
        <div className={GetFormationClass()} onClick={(e) => OpenChangeFormationModal(e, formationData.formation_id)}>
            
            <p>this is a formation with: </p>
            <p>goalkeeper: {formationData.goalkeeper}</p>
            <p>defence: {formationData.defence}</p>
            <p>middlefield: {formationData.middlefield}</p>
            <p>attack: {formationData.attack}</p>

        </div>
    );
}
 
export default Formation;