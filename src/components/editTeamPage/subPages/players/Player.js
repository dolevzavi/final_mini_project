import '../../../../styles/componentStyles/EditTeamPage/PlayerStyles.css'

const Player = (props) => {

    const {playerData} = {...props.data}

    return (
        <div className="card">
            <div className='player-name-container center-container'>
                <p className='player-name'>{playerData.name}</p>
            </div>
            <div className='player-rating-container center-container'>
                <p className='player-rating'>{playerData.rating}</p>
            </div>
            <div className='player-real-team-container center-container'>
                <p className='player-real-team'>{playerData.real_team}</p>
            </div>
        </div>
    );
}
 
export default Player;