import { useState, useEffect } from "react";
import useFetch from "../../../../hooks/useFetch"
import TeamPosition from "./TeamPosition";

const PlayersSubPage = (props) => {
      
    const {teamData, UpdateTeamData} = {...props.data}

    const IsPlayersDataFetched = () => {
        if (playersData && teamData) {
            // check if the number of players in playersData is similar to the number of players in teamData. because i want to check if the new fetch (after a player was added) is done
            let fetchedPlayersAmount = playersData.players_data['goalkeeper'].length + playersData.players_data['defence'].length + playersData.players_data['middlefield'].length + playersData.players_data['attack'].length;
            return (fetchedPlayersAmount == teamData.team_data.players.length)
        }
    }

    // fetching players data:

    // this is a function and not a single state because of a stupid error (something related to timing)
    const GetRequestPostData = () => {
        let requestPostData;
        return {'players_ids': teamData.team_data.players}
    }
    const [requestPostData, SetRequestPostData] = useState({'players_ids': teamData.team_data.players});
    const [requestUrl, SetRequestUrl] = useState('/get_players_data')
    let key = teamData;
    const {data: playersData, setData, isPending} = useFetch(requestUrl, GetRequestPostData(), [], key)

    const allPositions = ['attack', 'middlefield', 'defence', 'goalkeeper']

    return (
        <div className="players-subpage">
            {IsPlayersDataFetched() && allPositions.map((position) => {
                let teamPositionProps = {'teamData': teamData, 'UpdateTeamData': UpdateTeamData, 'playersData': playersData.players_data[position], 'position': position}
                return <TeamPosition data={teamPositionProps} key={position} />
            })}
        </div>
    );
}
 
export default PlayersSubPage;