import { useState } from 'react'
import AddPlayerModal from '../../../modals/AddPlayerModal'
import '../../../../styles/componentStyles/EditTeamPage/EmptyPlayerStyles.css'

const EmptyPlayer = (props) => {

    const {position, teamData, UpdateTeamData} = {...props.data}

    const OpenAddPlayerModal = (e) => {
        e.preventDefault()
        SetShowAddPlayerModal(true)
    }

    const CloseAddPlayerModal = (e) => {
        e.preventDefault()
        console.log('i ran')

        SetShowAddPlayerModal(false)
        console.log('showaddplayermodal: ', showAddPlayerModal)
    }

    const [showAddPlayerModal, SetShowAddPlayerModal] = useState(false)
    const addPlayerModalProps = {'teamData': teamData, 'UpdateTeamData': UpdateTeamData, 'position': position, 'CloseAddPlayerModal': CloseAddPlayerModal}

    return ( 
        <div className="empty-card center-container" onClick={(e) => {OpenAddPlayerModal(e)}}>
            <p className='empty-card-message'>add player</p>
            
            {showAddPlayerModal && <AddPlayerModal data ={addPlayerModalProps} />}
        </div>
    );
}
 
export default EmptyPlayer;