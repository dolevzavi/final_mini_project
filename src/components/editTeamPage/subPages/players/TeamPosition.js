import { cloneDeep } from 'lodash'
import Player from './Player'
import EmptyPlayer from './EmptyPlayer'
import '../../../../styles/componentStyles/EditTeamPage/TeamPositionStyles.css'

const TeamPosition = (props) => {

    const {teamData, UpdateTeamData, playersData, position} = {...props.data}

    const RemovePlayer = (e, playerId) => {
        e.preventDefault()
        let teamDataCopy = cloneDeep(teamData)

        // update 'players' field of the team
        let updatedTeamPlayers = teamDataCopy.team_data.players.filter(item => item !== playerId)

        // update 'positions' field of team
        let updatedTeamPositions = teamDataCopy.team_data.positions
        updatedTeamPositions[position] = updatedTeamPositions[position].filter(item => item !== playerId)

        UpdateTeamData([
            {'field': 'players', 'updatedValue': updatedTeamPlayers},
            {'field': 'positions', 'updatedValue': updatedTeamPositions}
        ])

    }

    const totalPlayersInPosition = teamData.team_data.formation[position]
    
    return (
        <div className="team-position-component">
            
            <div className='team-position-header center-container'>
                <p>{position}: </p>
            </div>

            <div className='team-position-players'>
                {[...Array.from({'length': totalPlayersInPosition}).keys()].map((index) => {
                    if(index < teamData.team_data.positions[position].length) {
                        return (
                            <div className='player-in-position' key={index}>
                                <Player data={{'playerData': playersData[index]}} />
                                <button className='simple-button' onClick={(e) => {RemovePlayer(e, playersData[index].player_id)}} >remove</button>
                            </div>
                        )
                    }
                    else {
                        let emptyPlayerProps = {'teamData': teamData, 'UpdateTeamData': UpdateTeamData, 'position': position}
                        return <EmptyPlayer data={emptyPlayerProps} key={index} />
                    }
                })}
            </div>

        </div>
    );
}
 
export default TeamPosition;