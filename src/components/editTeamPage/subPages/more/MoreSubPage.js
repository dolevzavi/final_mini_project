import { useState } from 'react'
import useFetch from '../../../../hooks/useFetch';

const MoreSubPage = (props) => {

    const {teamData, UpdateTeamData} = {...props.data}

    // fetching players (i need their names)
    const [requestPostData, SetRequestPostData] = useState({'players_ids': teamData.team_data.players});
    const [requestUrl, SetRequestUrl] = useState('/get_players_names')
    const {data: playersNames, setData, isPending} = useFetch(requestUrl, requestPostData, [])

    const GetNewRoles = (updatedRole) => {
        let changedField = updatedRole.field
        let newRolePlayerName = updatedRole.newRolePlayerName

        let newRolePlayerId = playersNames.players_names.find(item => item.name === newRolePlayerName).player_id
        
        let teamRolesCopy = {...teamData.team_data.roles}        
        teamRolesCopy[changedField] = newRolePlayerId

        return teamRolesCopy
    }

    const GetRolePlayerName = (role) => {
        if (playersNames.players_names.length === 0){
            return ''
        }
        else {
            let rolePlayerName = playersNames.players_names.find(item => item.player_id === teamData.team_data.roles[role])
            if (rolePlayerName) {
                return rolePlayerName.name
            }
            else {
                return ''
            }
        }
    }

    const HandleInputOnChange = (e, role) => {
        if (e.target.value == -1) {
            UpdateTeamData([{
                'field': 'roles',
                'updatedValue': -1  //should change this to id, not name(or maybe its ok)
            }])
          }
          else {
            UpdateTeamData([{
                'field': 'roles',
                'updatedValue': GetNewRoles({'field': role, 'newRolePlayerName': e.target.value})  //should change this to id, not name(or maybe its ok)
            }])
          }
    }


    const allRoles = ['captain', 'penalties', 'left_corner', 'right_corner', 'right_free_kick', 'left_free_kick']

    return (
        <div className='more-subpage-component'>

            {!isPending && allRoles.map((role) => {
                let rolePlayerName = GetRolePlayerName(role)
                return (
                    <div className='role-input-container' key={role}>
                        <p>{role}: </p>
                        <select 
                          value={rolePlayerName}
                          onChange={(e) => { HandleInputOnChange(e, role)}}
                        >
                            {/* all options */}
                            <option value={-1}>any</option>
                            {playersNames.players_names.map((playerObj) => {
                                return <option key={playerObj.player_id} value={playerObj.name}>{playerObj.name}</option> 
                            })}
                        </select>
                    </div>
                )
            })}

        </div>
    );
}
 
export default MoreSubPage;