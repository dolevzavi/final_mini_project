const ChangeFormationModal = (props) => {

    const {CloseChangeFormationModal, ChangeFormation, formationId} = {...props.data}
    console.log('props of modal: ', props)

    return (
        <div className="change-formation-component">
            <div className="overlay center-container">
                <div className="simple-modal">

                    <div className="simple-modal-message-container center-container">
                        <p>if you change formation all your players will be remove</p>
                    </div>

                    <div className="simple-modal-buttons-container">
                        <div className="simple-modal-single-button-container center-container">
                            <button className="simple-button" onClick={(e) => {CloseChangeFormationModal(e)}}>cancel</button>
                        </div>
                        <div className="simple-modal-single-button-container center-container">
                            <button className="simple-button" onClick={(e) => {ChangeFormation(e, formationId)}}>change formation</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
}
 
export default ChangeFormationModal;