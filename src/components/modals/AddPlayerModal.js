import { useState } from 'react'
import useFetch from '../../hooks/useFetch'
import Player from '../editTeamPage/subPages/players/Player'
import '../../styles/componentStyles/EditTeamPage/AddPlayerModalStyles.css'

const AddPlayerModal = (props) => {

    const {teamData, UpdateTeamData, position, CloseAddPlayerModal} = {...props.data}

    const AddPlayer = (e, playerId) => {
        let selectedPlayersCopy = [...teamData.team_data.players]
        selectedPlayersCopy.push(playerId)

        let positionsCopy = {...teamData.team_data.positions}
        positionsCopy[position].push(playerId)

        CloseAddPlayerModal(e)
        UpdateTeamData([
            {'field': 'players', 'updatedValue': selectedPlayersCopy},
            {'field': 'positions', 'updatedValue': positionsCopy}
        ])
    }

    // fetching unselected players (no afterfunctions needed)
    const [requestPostData, SetRequestPostData] = useState({
        'position': position,
        'selected_players': teamData.team_data.players
    })

    const [requestUrl, SetRequestUrl] = useState('/get_unselected_players')
    const {data: unselectedPlayersData, setData, isPending} = useFetch(requestUrl, requestPostData, [])

    return (
        <div className="add-player-modal-component">
            <div className="overlay center-container">
                <div className="simple-modal">

                    <div className="modal-message-container center-container">
                        <p>this is modal message</p>

                    </div>

                    <div className="modal-players-container">
                        {!isPending && unselectedPlayersData.unselected_players.map((playerData) => {
                            return (
                                <div className='modal-player-container' key={playerData.player_id}>
                                    <div className='player-container center container'>
                                        <Player data={{'playerData': playerData}} />
                                    </div>
                                    
                                    <div className='add-playerbutton-container center-container'>
                                        <button className='simple-button' onClick={(e) => {AddPlayer(e, playerData.player_id)}}>add player</button>
                                    </div>
                                </div>
                            )
                        })}

                    </div>

                    <div className='modal-buttons-container center-container'>
                        <button className='simple-button' onClick={(e) => {CloseAddPlayerModal(e)}}>cancel</button>
                    </div>

                </div>
            </div>
        </div>
    );
}
 
export default AddPlayerModal;