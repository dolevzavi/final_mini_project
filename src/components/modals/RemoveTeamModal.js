import { useState } from 'react';
import useFetch from '../../hooks/useFetch'

const RemoveTeamModal = (props) => {

    const { teamId, CloseRemoveTeamModal, RemoveTeam } = { ...props.data }

    const [showButtons, SetShowButtons] = useState(true)

    const TryFunc = (teamId) => {
        SetShowButtons(false)
        RemoveTeam(teamId)
    }

    return (
        <div className="remove-modal-component">
            <div className="overlay center-container">
                <div className="simple-modal">
                    
                    <div className="simple-modal-message-container center-container">
                        <p>are you sure you want to delete this team?</p>
                    </div>

                    {showButtons && <div className="simple-modal-buttons-container">
                        <div className="simple-modal-single-button-container center-container">
                            <button className="simple-button" onClick={() => TryFunc(teamId)}>remove</button>
                        </div>
                        <div className="simple-modal-single-button-container center-container">
                            <button className="simple-button" onClick={() => CloseRemoveTeamModal()}>cancel</button>
                        </div>
                    </div>}

               </div>
            </div>
        </div>
    );
}
 
export default RemoveTeamModal;