import '../../styles/componentStyles/TeamsPage/CreateTeamModalStyles.css'
import { useState } from "react";

const CreateTeamModal = (props) => {

    const {CloseCreateTeamModal, CreateTeam} = {...props.data}

    const [teamName, SetTeamName] = useState('')

    return (
        <div className="create-team-modal-component">
            <div className="overlay center-container">
                <div className="simple-modal">

                    <div className="simple-modal-message-container center-container">
                        <p>create team</p>
                    </div>

                    <div className="modal-input-container center-container">
                        <div className='name-header-container center-container'>
                            <p className='name-header'>name</p>
                        </div>
                        <div className='input-container center-container'>
                            <input
                              className="simple-text-input team-name-text-input"
                              type="text"
                              required
                              value={teamName} 
                              onChange={(e) => SetTeamName(e.target.value)} 
                            />
                        </div>

                    </div>

                    <div className="simple-modal-buttons-container">
                        <div className="simple-modal-single-button-container center-container">
                            <button className="simple-button" onClick={(e) => CloseCreateTeamModal(e)}>cancel</button>
                        </div>          
                        <div className="simple-modal-single-button-container center-container">
                            <button className="simple-button" onClick={(e) => CreateTeam(e, teamName)}>create</button>
                        </div>      
                    </div>

                </div>
            </div>
        </div>
    );
}
 
export default CreateTeamModal;