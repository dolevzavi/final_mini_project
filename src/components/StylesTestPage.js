import Player from "./editTeamPage/subPages/players/Player";

const StylesTestPage = () => {
    const playerProps = {
        "name": "rodriguez",
        "player_id": 4,
        "position": "middlefield",
        "rating": 87,
        "real_team": "hapoel tel aviv"
    }

    return (
        <div className="styles-test-page-component">
            <Player data={playerProps} />
        </div>
    );
}
 
export default StylesTestPage;