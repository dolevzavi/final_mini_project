import { useState, useEffect } from "react"


const useFetch = (url, postData, afterFunctions, key='bla') => {

    const runAfterFunctions = (data, setData, isPending) => {
        
        // the structure of the prop 'afterFunctions' is like this: 
        // [{'function': Function, 'params': ['data'/'setData'/'isPending']}, ...]

        afterFunctions.map((functionObject) => {
            
            // check which parameters should be passed to each function
            let parametersForFunction = functionObject.params;
            if (functionObject.fetchParams.includes('data')) {
                parametersForFunction.push(data)
            }
            if (functionObject.fetchParams.includes('setData')) {
                parametersForFunction.push(setData)
            }
            if (functionObject.fetchParams.includes('isPending')) {
                parametersForFunction.push(isPending)
            }

            // runs the functions with the right paramteres
            functionObject.function(...parametersForFunction)

            return '' // to ignore return from map functino error
        })
    }

    const [data, setData] = useState(null) // the data got back from the server
    const [isPending, setIsPending] = useState(true) // state of fetch

    useEffect(() => {
        // console.log('this fetch is made: ', url, 'with this postData: ', postData)
        if (url !== '/') {
            setTimeout(() => {
                fetch(url, {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify(postData)
                }).then(res => {
                    if (!res.ok) {
                        throw Error('error: could not fetch the data')
                    }
                    return res.json()
                }).then((data) => {
                    setData(data)
                    setIsPending(false)
                    runAfterFunctions(data, setData, isPending)
                }).catch(err => console.log(err.message))

            }, 500)
        }
    }, [url, key]) // the Date ensure that the useFetch will run every time a component that makes a fetch is rendered (even if the url value stays the same)

    return { data, setData, isPending }
}

export default useFetch;