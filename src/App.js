// import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import TeamsPage from './components/pageComponents/TeamsPage';
import EditTeamPage from './components/pageComponents/EditTeamPage';
import StylesTestPage from './components/StylesTestPage';

function App() {
  return (
    <Router>
        <Switch>

            <Route exact path="/teams">
                <TeamsPage />
            </Route>

            <Route exact path='/edit_team/:teamId'>
                <EditTeamPage />
            </Route>

            <Route exact path="/styles_test">
                <StylesTestPage />
            </Route>

        </Switch>
    </Router>
  );
}

export default App;
