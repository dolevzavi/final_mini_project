from flask import Flask, request, jsonify
import functions as funcs


app = Flask(__name__)
app.secret_key = 'doesnt matter what the secret key is'


@app.route('/get_teams', methods=['GET', 'POST'])
def get_teams():
    # return data of all teams
    teams_dict = funcs.db_get_teams()
    return jsonify({'teams_list': teams_dict})


@app.route('/remove_team', methods=['GET', 'POST'])
def remove_team():
    team_id = request.get_json()['teamId']
    funcs.db_remove_team(team_id)
    return jsonify({})


@app.route('/get_team_data', methods=['GET', 'POST'])
def get_team_data():
    team_id = request.get_json()['team_id']
    fields = request.get_json()['fields']
    team_data = funcs.db_get_team_data(team_id, fields)

    return jsonify({'team_data': team_data})


@app.route('/set_team_data', methods=['GET', 'POST'])
def set_team_data():
    # the data should look like this:
    # {'team_id': team_id, 'updated_data': {'updated_data': {'name': team's_name...}}} - (the updated_fields parameter only contains fields that should be changed)
    team_id = request.get_json()['team_id']
    updated_fields = request.get_json()['updated_data']['team_data']

    funcs.db_set_team_data(team_id, updated_fields)
    
    return jsonify({})

@app.route('/get_players_data', methods=['GET', 'POST'])
def get_players_data():
    # this function gets a list of ids and return their data
    players_ids = request.get_json()['players_ids']
    players_data = funcs.db_get_players_data(players_ids)

    return jsonify({'players_data': players_data})

@app.route('/get_unselected_players', methods=['GET', 'POST'])
def get_unselected_players():
    position = request.get_json()['position']
    selected_players = request.get_json()['selected_players']

    unselected_players = funcs.db_get_unselected_players(position, selected_players)
    return jsonify({'unselected_players': unselected_players})

@app.route('/create_new_team', methods=['GET', 'POST'])
def create_new_team():
    team_name = request.get_json()['name']
    funcs.db_create_new_team(team_name)
    return jsonify({})


@app.route('/get_players_names', methods=['GET', 'POST'])
def get_players_names():
    print('got here at least')
    players_ids = request.get_json()['players_ids']
    players_names = funcs.db_get_players_names(players_ids)
    print('this is players_names: ', players_names)

    return jsonify({'players_names': players_names})

if __name__ == '__main__':
    app.run(debug=True)
