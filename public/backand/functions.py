from pymongo import MongoClient


connection_string = r'mongodb://localhost:27017'
client = MongoClient(connection_string)
db = client.final_mini_project


def generate_new_id(collection, id_field_name):
    objects_list = list(db[collection].find({}, {id_field_name: 1}))
    ids_list = [item[id_field_name] for item in objects_list]
    return max(ids_list) + 1

def calculate_team_rating(players_ids):
    players_list = list(db.players.find({'player_id': {'$in': players_ids}}, {'rating': 1}))
    ratings_list = [player['rating'] for player in players_list]
    return int(sum(ratings_list) / 11)
    

def db_get_teams():
    # fetching all teams in the collection
    return list(db.teams.find({}, {'_id': 0}))


def db_remove_team(team_id):
    db.teams.delete_one({'team_id': team_id})


def db_get_team_data(team_id, fields):
    # query_fields contain the fields i want to fetch, it looks like this: {'_id': 0, 'name': 1, 'rating': 1...} (fields that I don't want to fetch are not in the dict)
    query_fields = {'_id': 0}
    for field in fields:
        query_fields[field] = 1

    return db.teams.find_one({'team_id': int(team_id)}, query_fields)


def db_set_team_data(team_id, updated_fields):
    print('this is updated fields: ', updated_fields)
    updated_fields['rating'] = calculate_team_rating(updated_fields['players'])
    db.teams.update_one({'team_id': int(team_id)}, {'$set': updated_fields})


def db_get_players_data(players_ids):
    players_data = list(db.players.find({'player_id': {'$in': players_ids}}, {'_id': 0}))

    players_data_by_position = {'goalkeeper': [], 'defence': [], 'middlefield': [], 'attack': []}
    for player in players_data:
        player_position = player['position']
        players_data_by_position[player_position].append(player)

    return players_data_by_position


def db_get_unselected_players(position, selected_players):
    unselected_players = list(db.players.find({'player_id': {'$not': {'$in': selected_players}}, 'position': position}, {'_id': 0}))
    return unselected_players


def db_create_new_team(team_name):
    # should add roles to default team toooooooooooooooo
    team_created = {
        'name': team_name,
        'rating': 0,
        'team_id': generate_new_id('teams', 'team_id'),
        'formation': {
            'formation_id': 4,
            'name': "4-3-3",
            'goalkeeper': 1,
            'defence': 4,
            'middlefield': 3,
            'attack': 3,
        },
        'players': [],
        'roles': {
            'captain': -1,
            'left_corner': -1,
            'left_free_kick': -1,
            'penalties': -1,
            'right_corner': -1,
            'right_free_kick': -1
        },
        'positions': {
            'goalkeeper': [],
            'defence': [],
            'middlefield': [],
            'attack': []
        }
    }
    db.teams.insert_one(team_created)


def db_get_players_names(players_ids):
    players_names = list(db.players.find({'player_id': {'$in': players_ids}}, {'name': 1, 'player_id': 1, '_id': 0}))
    return players_names